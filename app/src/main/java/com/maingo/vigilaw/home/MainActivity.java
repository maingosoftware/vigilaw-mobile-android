package com.maingo.vigilaw.home;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.maingo.vigilaw.R;
import com.maingo.vigilaw.base.BaseActivity;
import com.maingo.vigilaw.repos.RouteListFragment;

public class MainActivity extends BaseActivity implements RouteListFragment.OnFragmentInteractionListener {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected Fragment initialScreen() {
        return RouteListFragment.newInstance();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.e("Borrar", "Esto...");
    }
}

package com.maingo.vigilaw.data;

import com.maingo.vigilaw.model.Repo;
import com.maingo.vigilaw.networking.RepoService;

import java.util.List;

import javax.inject.Inject;
import retrofit2.Call;

public class RepoRequester {

    private final RepoService service;

    @Inject
    RepoRequester(RepoService service) {
        this.service = service;
    }

    public Call<List<Repo>> getRepositories() {
        return service.getRepositories();
    }
}

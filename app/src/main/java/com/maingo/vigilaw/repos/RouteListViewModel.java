package com.maingo.vigilaw.repos;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.VisibleForTesting;

import com.maingo.vigilaw.data.RepoRepository;
import com.maingo.vigilaw.model.Repo;

import java.util.List;

import javax.inject.Inject;

public class RouteListViewModel extends ViewModel {

    private final RepoRepository repoRepository;
    private MutableLiveData<Boolean> loading;

    @Inject
    public RouteListViewModel(RepoRepository repoRepository) {
        this.repoRepository = repoRepository;
        this.loading = repoRepository.getLoading();
        fetchRepos();
    }

    LiveData<List<Repo>> getRepos() {
        return repoRepository.getRepos();
    }

    LiveData<Boolean> getError() {
        return repoRepository.getError();
    }

    LiveData<Boolean> getLoading() {
        return loading;
    }

    private void fetchRepos() {
        repoRepository.apiRepositories();
    }

    @Override
    protected void onCleared() { // Fragment no longer on back stack or being displayed
        repoRepository.clearRepoCall();
    }

    @VisibleForTesting
    protected void changeLoadingValue(boolean bool) {
        this.loading.setValue(bool);
    }

    @VisibleForTesting
    protected void setLoadingInstance(MutableLiveData<Boolean> loadingInstance) {
        this.loading = loadingInstance;
    }

}

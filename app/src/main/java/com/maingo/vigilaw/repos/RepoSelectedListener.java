package com.maingo.vigilaw.repos;

import com.maingo.vigilaw.model.Repo;

public interface RepoSelectedListener {

    void onRepoSelected(Repo repo);

}

package com.maingo.vigilaw.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.maingo.vigilaw.repos.RouteListViewModel;
import com.maingo.vigilaw.repos.SelectedRepoViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(RouteListViewModel.class)
    abstract ViewModel bindRouteListViewModel(RouteListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectedRepoViewModel.class)
    abstract ViewModel bindSelectedRepoViewModel(SelectedRepoViewModel viewModel);
}

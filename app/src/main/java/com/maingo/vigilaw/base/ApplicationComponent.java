package com.maingo.vigilaw.base;

import com.maingo.vigilaw.details.DetailsFragment;
import com.maingo.vigilaw.repos.RouteListFragment;
import com.maingo.vigilaw.networking.ServiceModule;
import com.maingo.vigilaw.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ServiceModule.class,
        ViewModelModule.class,
})
public interface ApplicationComponent {

    void inject(RouteListFragment routeListFragment);

    void inject(DetailsFragment detailsFragment);

}

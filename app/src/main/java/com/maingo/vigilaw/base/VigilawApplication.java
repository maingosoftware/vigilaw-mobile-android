package com.maingo.vigilaw.base;

import android.app.Application;
import android.content.Context;

import com.maingo.vigilaw.BuildConfig;

import timber.log.Timber;

public class VigilawApplication extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.create();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static ApplicationComponent getApplicationComponent(Context context) {
        return ((VigilawApplication) context.getApplicationContext()).component;
    }
}

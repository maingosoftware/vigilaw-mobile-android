package com.maingo.vigilaw.repos;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import com.maingo.vigilaw.data.RepoRepository;
import com.maingo.vigilaw.model.Repo;
import com.maingo.vigilaw.testutils.TestUtils;
import com.squareup.moshi.Types;

import junit.framework.Assert;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Unit test for {@link RouteListViewModel}
 */
@RunWith(MockitoJUnitRunner.class)
public class RouteListViewModelTest {

    // Executes each task synchronously using Architecture Components.
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private MutableLiveData<Boolean> booleanMutableLiveData;

    @Mock
    private RepoRepository repoRepository;

    @Mock
    private Observer<Boolean> observer;

    private RouteListViewModel viewModel;

    @Before
    public void setUp() throws Exception {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        viewModel = new RouteListViewModel(repoRepository);
    }

    @Test
    public void getRepos() {

        Type type = Types.newParameterizedType(List.class, Repo.class);
        List<Repo> response = TestUtils.loadJsonArray("mock/get_repos.json", type);
        Assert.assertEquals(11, response.size());

        //TODO Test the viewModel

    }

    @Test
    public void getError() {

        //TODO Test viewModel getError

    }

    @Test
    public void getLoading() {

        ArgumentCaptor<Boolean> booleanCaptor = ArgumentCaptor.forClass(Boolean.class);
        viewModel.setLoadingInstance(new MutableLiveData<>());
        viewModel.getLoading().observeForever(observer);

        viewModel.changeLoadingValue(true);
        viewModel.changeLoadingValue(false);

        verify(observer, times(2)).onChanged(booleanCaptor.capture());
        List<Boolean> capturedPeople = booleanCaptor.getAllValues();
        Assert.assertEquals(Boolean.TRUE, capturedPeople.get(0));
        Assert.assertEquals(Boolean.FALSE, capturedPeople.get(1));

    }

}
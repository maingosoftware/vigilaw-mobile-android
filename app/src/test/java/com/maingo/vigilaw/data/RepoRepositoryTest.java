package com.maingo.vigilaw.data;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.inject.Provider;

public class RepoRepositoryTest {

    @Mock
    Provider<RepoRequester> repoRequesterProvider;
    @Mock RepoRequester repoRequester;
    private RepoRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(repoRequesterProvider.get()).thenReturn(repoRequester);

        repository = new RepoRepository(repoRequesterProvider);
    }

    @Test
    public void getRepos() {
    }

    @Test
    public void getError() {
    }

    @Test
    public void getLoading() {
    }

    @Test
    public void apiRepositories() {
    }
}